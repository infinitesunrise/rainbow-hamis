--replaces all spawned hamis with rainbow-colored hamis
function OnWorldPreUpdate() 
	player = EntityGetWithTag("player_unit")[1]
	if player then
		x, y = EntityGetTransform(player)
		local enemies = EntityGetInRadiusWithTag(x, y, 256, "enemy")
		for i = 1, #enemies, 1 do
			local enemy = enemies[i]
			if EntityHasTag(enemy, "rainbow_hamis") == false and EntityGetName(enemy) == "$animal_longleg" then
				colorRoll = math.random(6)
				eX, eY = EntityGetTransform(enemy)
				if colorRoll == 1 then
					newHamis = EntityLoad("data/entities/animals/longleg_red.xml", eX, eY)
				elseif colorRoll == 2 then
					newHamis = EntityLoad("data/entities/animals/longleg_orange.xml", eX, eY)
				elseif colorRoll == 3 then
					newHamis = EntityLoad("data/entities/animals/longleg_yellow.xml", eX, eY)
				elseif colorRoll == 4 then
					newHamis = EntityLoad("data/entities/animals/longleg_green.xml", eX, eY)
				elseif colorRoll == 5 then
					newHamis = EntityLoad("data/entities/animals/longleg_blue.xml", eX, eY)
				elseif colorRoll == 6 then
					newHamis = EntityLoad("data/entities/animals/longleg.xml", eX, eY)
				end
				EntityAddTag(newHamis, "rainbow_hamis")
				EntityKill(enemy)
			end
		end
	end
end

--stacks the enemy spawn table of all main path biomes with large groups of hamis
ModLuaFileAppend( "data/scripts/biomes/coalmine.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/coalmine_alt.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/excavationsite.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/snowcave.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/snowcastle.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/rainforest.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/vault.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/crypt.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )
ModLuaFileAppend( "data/scripts/biomes/fungicave.lua", "mods/rainbow_hamis/scripts/add_rainbow_hamis.lua" )